# WSDocker

Outil de gestion d'un hôtel allant la recherche à l'annulation en passant par la réservation .

## Installation 

```bash
git clone https://gitlab.com/SamirPS/wsdocker.git
cd wsdocker
docker-compose up
```

Le client est disponible ici   http://localhost:80 .

## Structure du projet 

Les dossiers sont  : 
* **Client** contient le code source du client flask.
* **Db** contient la base de données de test . 
* **Serveur** contient le code source du rest fait sous  flask. 
* **War** Contient le fichier war du SOAP.



